import React, { FunctionComponent, useState } from 'react';
import * as Demo from './demo/demo1-basics';
import { RecoilRoot } from 'recoil';

const obj: {[key: string]: FunctionComponent } = {
  Demo1: Demo.Demo1Basics,
  // Demo2: Demo.Demo2,
  Demo3: Demo.Demo3,
  Demo4: Demo.Demo4,
  Demo5: Demo.Demo5,
  Demo6: Demo.Demo6,
  Demo7: Demo.Demo7,
  Demo7CrudWithFilters: Demo.Demo7CrudWithFilters,
  Demo8: Demo.Demo8,
  Demo9: Demo.Demo9,
  Demo10: Demo.Demo10,
  Demo11: Demo.Demo11,
  Demo12: Demo.Demo12,
}
function App() {
  const [currentDemo, setCurrentDemo] = useState<string>('Demo9')

  const Demo: any = React.createElement(obj[currentDemo])

  return (
    <RecoilRoot>
      <select onChange={(e) => setCurrentDemo(e.target.value)} value={currentDemo}>
        {
          Object.keys(obj).map((key, index) => {
            return <option value={key} key={index}>{key}</option>
          })
        }
      </select>
      <hr/>
      {Demo}
    </RecoilRoot>
  );
}

export default App;
