import React  from 'react';
import { atom, useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

/**
 * Description
 * Read / Write state with recoil
 */
const valueState = atom<string>({
  key: 'valueState',
  default: '',
});

export function Demo1Basics() {
  return <div>
    <UserForm />
    <ShowValue />
    <WriteValue />
  </div>
}


function UserForm() {
  const [value, setValue] = useRecoilState(valueState)
  return (
    <>
      <h1>{value}</h1>
      <input type="text" value={value} onChange={e => setValue(e.target.value)} placeholder="write anything"/>
    </>
  )
}

function ShowValue() {
  const value = useRecoilValue(valueState)
  return (
    <>
      <hr/>
      { value ? <h4>Value is {value}</h4> : 'no value'}
    </>
  )
}

function WriteValue() {
  const setValue = useSetRecoilState(valueState)
  return (
    <>
      <hr/>
      <button onClick={() => setValue('pippo')}>Write PIppo</button>
    </>
  )
}
