import React, { ChangeEvent, FormEvent, useState } from 'react';
import { atom, selector, useRecoilState, useRecoilValue } from 'recoil';

/**
 * Description
 * Multiple form inputs (text and checkbox)
 * Another way to use selectors and manipulate the returned state
 */
type User = { name: string, subscribe: boolean }

const usersState = atom<User[]>({
  key: 'usersState',
  default: [],
});

const subscribedUsers = selector<number>({
  key: 'remainingFreeUsers',
  get: ({get}) => {
    return get(usersState).filter(u => u.subscribe).length
  },
})

///// ROOT COMPO /////
export const Demo4: React.FC = () => {
  return <div>
    <UserForm />
    <UsersList />
  </div>
};

///// COMPONENTS /////
function UserForm() {
  const [data, setData] = useState<User>({ name: '', subscribe: false })
  const [users, setUsers] = useRecoilState<User[]>(usersState)

  function submit(e: FormEvent) {
    e.preventDefault();
    setUsers([...users, { name: data.name, subscribe: data.subscribe }]);
    setData({ ...data, name: '' })
  }

  function onChanged(e: ChangeEvent<HTMLInputElement>) {
    setData({
      ...data,
      [e.currentTarget.name]: e.currentTarget.type === 'checkbox' ? e.currentTarget.checked : e.currentTarget.value
    })
  }

  return (
    <form onSubmit={submit}>
      <div>subscribe newsletter: <input type="checkbox" checked={data.subscribe} name="subscribe" onChange={onChanged}  /></div>
      <input type="text" value={data.name} name="name" onChange={onChanged} placeholder="Write & press ENTER" />
    </form>
  )
}

function UsersList() {
  const users = useRecoilValue<User[]>(usersState);
  const subUsers = useRecoilValue<number>(subscribedUsers);
  return <div>
    {subUsers} subscribed users of {users.length}
  </div>
}
