import React, { FormEvent, useState } from 'react';
import {
  atom,
  DefaultValue,
  selector,
  useRecoilState,
  useRecoilValue,
  useResetRecoilState
} from 'recoil';

/**
 * Description
 * Use the set method in selectors
 */
type User = { name: string, subscribe: boolean }

///// ATOMs /////
const usersState = atom<User[]>({
  key: 'usersState5',
  default: [],
});

///// SELECTORs /////
const usersTotal = selector<number>({
  key: 'usersTotal5',
  get: ({get}) => {
    return get(usersState).length
  },
})

///// SELECTORs /////
const usersStateSelector = selector<User[]>({
  key: 'usersSelector',
  get: ({get}) => {
    return get(usersState)
  },
  set: ({set, get}, newUsers) => {
    if (newUsers instanceof DefaultValue) {
      set(usersState, []);
      return;
    }

    if (get(usersTotal) < get(maxAvailableSeats) ) {
      console.log('newm', newUsers)
      set(usersState, prevState => prevState.concat(newUsers))
    }

  }
})

const maxAvailableSeats = atom<number>({
  key: 'availableSeats5',
  default: 3,
});


const availableSeats = selector<number>({
  key: 'remainingUsers5',
  get: ({get}) => {
    return get(maxAvailableSeats) - get(usersState).length
  },
  set: ({set, get}, newMax) => {

    // reset ( DefaultValue is {} when atom is resetted)
    if (newMax instanceof DefaultValue) {
      set(usersState, [])
      return;
    }

    // Remove elements at the end of the list when max changes (if Max is <= totalUsers
    const users = get(usersState)
    if (newMax < users.length) {
      const newUsers = users.slice(0, newMax)
      set(usersState, newUsers);
    }
    set(maxAvailableSeats, newMax)
  }
})

///// ROOT COMPO /////
export const Demo5: React.FC = () => {
  return <div>
    <h1>Demo5</h1>
    <UserForm />
    <UsersList />
  </div>
};

///// COMPONENTS /////
function UserForm() {
  const [value, setValue] = useState<string>('')
  const [users, setUsers] = useRecoilState<User[]>(usersStateSelector)

  function submit(e: FormEvent) {
    e.preventDefault();
    setUsers([{ name: value, subscribe: true }]);
    setValue('')
  }

  return (
    <form onSubmit={submit}>
      <input type="text" value={value} onChange={e => setValue(e.target.value)} placeholder="Write something and press ENTER" />
    </form>
  )
}

function UsersList() {
  // users list
  const users = useRecoilValue<User[]>(usersState);
  // total users
  const totalUsers = useRecoilValue<number>(usersTotal);
  // max available seats
  const [maxAvailableSeats, setMaxAvailableSeats] = useRecoilState<number>(availableSeats);
  // reset available seats
  const resetAvailableSeats = useResetRecoilState(availableSeats);

  return <div>
    <div>There are {totalUsers} users ( {maxAvailableSeats} availble seat)</div>
    {
      users.map((u, index) => {
        return <li key={index}>{u.name}</li>
      })
    }
    <button onClick={() => setMaxAvailableSeats(3)}>Change Max to 3</button>
    <button onClick={() => setMaxAvailableSeats(5)}>Change Max to 5</button>
    <button onClick={resetAvailableSeats}>reset</button>
  </div>
}
