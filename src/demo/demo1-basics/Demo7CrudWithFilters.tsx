import React from 'react';
import {
  atom,
  selector, useRecoilState,
  useRecoilValue,
} from 'recoil';
import { ErrorBoundary } from './ErrorBoundary';

/**
 * Description: ASYNC and 2 selectors to get data (the main state and a filter)
 */
type Genders = 'all' | 'M' | 'F';
type User = { id: number, name: string, age: number, gender: Genders }

///// USERS /////
const usersState = selector<User[]>({
  key: 'users',
  get: async ({ get }) => {
      try {
        const response = await fetch('http://localhost:3001/users')

        if (response.status !== 200) {
          return []
        }
        return response.json();
      } catch(e) {
        return []
      }
  }
})

// 389 6568434
const usersStateSelector = selector<User[]>({
  key: 'usersStateSelector',
  get: ({get}) => {
    const users = get(usersState);
    const filters = get(usersFilters);

    return users.filter(
      user =>
        +user.age > +filters.age &&
        (user.gender === filters.gender || filters.gender === 'all')
    )
  }
});

///// FILTERS /////
type usersFilterChoice = { gender: Genders, age: number};

const usersFilters = atom<usersFilterChoice>({
  key: 'usersFilterSelector',
  default: {
    gender: 'all',
    age: 0
  }
})

const usersFiltersSelector = selector<Partial<usersFilterChoice>>({
  key: 'MySelector',
  get: ({get}) => get(usersFilters),
  set: ({get, set}, value) => {
    set(usersFilters, prevState => ({...prevState, ...value}));
  }
});

///// ROOT COMPO /////
export const Demo7CrudWithFilters: React.FC = () => {
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <ErrorBoundary>
        <UserFilters />
        <UsersList />
      </ErrorBoundary>
    </React.Suspense >
  )
};

////// CHILDREN COMPONENTS: LIST + FILTERS /////
function UserFilters() {
  const [filters, setFilters] = useRecoilState(usersFiltersSelector)

  function setGender(e: React.ChangeEvent) {
    const gender = (e.target as HTMLSelectElement).value as Genders;
    setFilters({gender: gender })
  }
  return (
    <div>
      <select value={filters.gender} onChange={setGender} >
        <option value="all">All genders</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      Min Age:
      <input type="number" min={0} max={50} value={filters.age} onChange={e => setFilters({age: +e.target.value})} placeholder="Write something and press ENTER" />
    </div>
  )
}

function UsersList() {
  const users = useRecoilValue(usersStateSelector)

  return <div>
    {
      users.map((u: User, index: number) => {
        return <li key={index}>{u.name}</li>
      })
    }
  </div>
}
