import React  from 'react';
import { selectorFamily, useRecoilValue } from 'recoil';
import { ErrorBoundary } from './ErrorBoundary';

/**
 * Description
 * multiple async data query with params
 * NOTE: simulate different delay for any request
 * PROBLEM: it take too much time. See next example to see how the `wait` util is used
 */
const fetchUsers = (id: number) => {
  return new Promise((res, rej) => {
    setTimeout(async () => {
      try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users/' + id);
        // return await response.json()
        console.log(response)
        if (response.status === 404) {
          rej('user not found');
        }
        res(response.json())
      } catch (e) {
        // throw 'user not found'
        console.log('error', id)
        rej('server error');
      }
    }, 500 * id)
  })
}

const userNameQuery = selectorFamily<any, any>({
  key: 'UserName',
  get: (userID: number) => async () => {
    return fetchUsers(userID);
  },
});

export function Demo9() {
  return <div>
    Demo 9: wait 11 seconds to get the result
    <hr/>
      <React.Suspense fallback={<div>Loading...</div>}>
        <UserInfo userID={1}/>
        <UserInfo userID={2}/>
        <UserInfo userID={3}/>
        <ErrorBoundary>
         <UserInfo userID={11}/>
        </ErrorBoundary>
      </React.Suspense>

  </div>
}


function UserInfo({userID} : {userID: number}) {
  const userName = useRecoilValue<{ name: string }>(userNameQuery(userID));
  return <div>
      {userName.name}
    </div>;
}
