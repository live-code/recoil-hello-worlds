import React  from 'react';
import { atomFamily, useRecoilValue, useSetRecoilState } from 'recoil';

/**
 * Description
 * Use atomFamily to handle local states to a series of similar elements
 */
const elementPositionStateFamily = atomFamily<number,  number>({
  key: 'ElementPosition',
  default: 0
});

export function Demo11() {
  return <div>
    <ElementListItem elementID={1} />
    <ElementListItem elementID={2} />
  </div>
}


function ElementListItem({elementID} : { elementID: number}) {
  const position = useRecoilValue(elementPositionStateFamily(elementID));
  const setPosition = useSetRecoilState(elementPositionStateFamily(elementID));
  console.log('render', elementID)
  return (
    <div>
      Element: {elementID}
      Position: {position}
      <button onClick={() => setPosition(Math.random())}> Random Value</button>
    </div>
  );
}
