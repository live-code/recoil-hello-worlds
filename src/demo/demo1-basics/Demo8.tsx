import React from 'react';
import {
  atom, selector,
  selectorFamily,
  useRecoilState,
  useRecoilValue, useSetRecoilState,
} from 'recoil';

/**
 * Description
 * Use selectorFamily
 */
const gender = atom<string>({
  key: 'gender',
  default: 'M',
});

const attendees = atom<number>({
  key: 'attendees',
  default: 1,
});

///// ROOT COMPO /////

export const Demo8: React.FC = () => {
  return <div>
    <SimpleSelector />
    <hr/>
    <SelectorFamily seatPerTable={10}/>
  </div>
}

/// EXAMPLE with SELECTOR ////

const tables = selector< number>({
  key: 'MyMultipliedNumber',
  get: ({get}) => {
    console.log(get(attendees))
    return Math.ceil(get(attendees) / 5)
  },
  // optional set
  set: ({set}, newTableValue) => {
    console.log('set',  newTableValue)
    set(attendees, +newTableValue * 5)
  },
});


export const SimpleSelector: React.FC = () => {
  const [myAttendees, setMyAttendees] = useRecoilState(attendees)
  const [myTables, setMyTable] = useRecoilState(tables)

  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <div>
        <h1>Table Calculator</h1>
        <em>Every table has 5 seats</em>
        <div>max attendees: <input type="text" value={myAttendees} onChange={e => setMyAttendees(+e.target.value)}/></div>
        <br/>
        tables: <input type="range" min={1} max={20} value={myTables} onChange={e => setMyTable(+e.target.value)}/>
        {myTables}
      </div>
    </React.Suspense >
  )
};

/// EXAMPLE with SELECTOR FAMILY ////

const tablesWithSeat = selectorFamily< number, number>({
  key: 'MyMultipliedNumber',
  get: (maxSeatPerTable: number) => ({get}) => {
    console.log(maxSeatPerTable, get(attendees))
    return Math.ceil(get(attendees) / maxSeatPerTable)
  },
  // optional set
  set: (maxSeatPerTable: number) => ({set}, newTableValue) => {
    console.log('set', maxSeatPerTable, newTableValue)
    set(attendees, +newTableValue * maxSeatPerTable)
  },
});


///// ROOT COMPO /////
export const SelectorFamily: React.FC<{ seatPerTable: number}> = (props) => {
  const [myAttendees, setMyAttendees] = useRecoilState(attendees)
  const [myTables, setMyTable] = useRecoilState(tablesWithSeat(props.seatPerTable))

  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <div>
        <h1>Table Calculator</h1>
        <em>Every table has {props.seatPerTable} seats</em>
        <div>max attendees: <input type="text" value={myAttendees} onChange={e => setMyAttendees(+e.target.value)}/></div>
        <br/>
        tables: <input type="range" min={1} max={10} value={myTables} onChange={e => setMyTable(+e.target.value)}/>
        {myTables}
      </div>
    </React.Suspense >
  )
};
