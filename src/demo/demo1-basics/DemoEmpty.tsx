import React  from 'react';
import { atom, useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

/**
 * Description
 * Read / Write state with recoil
 */
const valueState = atom<string>({
  key: 'valueState',
  default: '',
});

export function Demo11() {
  return <div>
    Demo11
  </div>
}

