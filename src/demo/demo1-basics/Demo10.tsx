import React  from 'react';
import { selector, selectorFamily, useRecoilValue, waitForNone } from 'recoil';
import { ErrorBoundary } from './ErrorBoundary';

/**
 * Description
 * CONCURRENCY: async data query with params
 * NOTE: simulate different delay for any request
 * PROBLEM FIXED: UI is partially renderer when every user data is loaded
 */
const userNameQuery = selectorFamily<any, any>({
  key: 'UserName',
  get: (userID: number) => async () => {
    return new Promise((res, rej) => {
      setTimeout(async () => {
        try {
          const response = await fetch('https://jsonplaceholder.typicode.com/users/' + userID);
          // return await response.json()
          res(response.json())
        } catch (e) {
          // throw 'user not found'
          rej('user not found');
        }
      }, 2000 * userID)
    })
    /*if (response.error) {
      throw response.error;
    }
    return response.name;*/
  },
});

const usersQuery = selector({
  key: 'FriendsInfoQuery',
  get: ({get}) => {
    const userIds = [1, 2, 3, 4];
    const userLoadables = get(waitForNone(
      userIds.map(userID => userNameQuery(userID))
    ));
    return userLoadables
      .filter(({state}) => state === 'hasValue')
      .map(({contents}) => contents);
  },
});

function UserInfo(props: { name: string }) {
  // const userName = useRecoilValue<{ name: string }>(userNameQuery(userID));
  return <div>{props.name}</div>;
}
export function Demo10() {
  const friends = useRecoilValue(usersQuery);
  console.log(friends)
  return <div>
    Demo 10: wait 11 seconds to get the result
    <hr/>
    <ErrorBoundary>
      <React.Suspense fallback={<div>Loading...</div>}>
        {
          friends.map(user => {
            return <UserInfo key={user.id} name={user.name}/>
          })
        }
        {/*<UserInfo userID={11}/>*/}
      </React.Suspense>
    </ErrorBoundary>
  </div>
}

